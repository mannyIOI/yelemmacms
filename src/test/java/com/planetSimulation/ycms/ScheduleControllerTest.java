package com.planetSimulation.ycms;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.awt.List;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import com.planetSimulation.ycms.controllers.ScheduleController;
import com.planetSimulation.ycms.domains.*;
import com.planetSimulation.ycms.repositories.UserRepository;
import com.planetSimulation.ycms.security.User;
import com.planetSimulation.ycms.services.AppointmentService;
import com.planetSimulation.ycms.services.UserService;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(ScheduleController.class)
public class ScheduleControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	
	private ArrayList<Appointment> appointments;
	
	private Appointment appointemnt;
	
	@MockBean
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@MockBean
	private AppointmentService appointmentService;
	
	
	
	
	@MockBean
	private UserService userService;
	
	
	@Before
	public void setup() {
		User user= new User();
		user.setId(2L);
		user.setFirstName("fdoc");
		user.setLastName("ldoc");
		user.setUsername("doc");
		user.setAge(20);
		user.setEmail("email");
		user.setPassword("password");
		
		String strTime="14:00:00";
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
		Date time=cal.getTime();
		Date appointmentDate = new Date(Calendar.getInstance().getTimeInMillis());
		
		Appointment appointment= new Appointment();
		appointment.setAppointmentId(1);
		appointment.setPatientId(1);
		appointment.setDoctorId(2);
		appointment.setAppointmentTime(time);
		appointment.setAppointmentDate(appointmentDate);
		
		appointments.add(appointment);
		
		when(appointmentService.findAppointmentByAppointmentDateAndDoctorIdAndApproved(appointmentDate, 2, 1))
			.thenReturn(appointments);
		when(userService.findUserByUsername("doc"))
			.thenReturn(user);

	}

	@Test
	@WithMockUser(username="doc", password="password", authorities="DOCTORUSER")
	public void testShowSchedule() throws Exception {
		mockMvc
		
		.perform(get("/schedule"))
		.andExpect(status().isOk())
		
		.andExpect(view().name("schedule"))
		.andExpect(model().attribute("appointments",appointments));
		
	}

}
