package com.planetSimulation.ycms.services;

import java.util.ArrayList;
import java.util.Optional;

import com.planetSimulation.ycms.domains.LabExam;

public interface LabExamService {

	public LabExam save(LabExam labExam);
	
	Optional<LabExam> findById(Long id);
	
	ArrayList<LabExam> findLabExamByApproved(int approved);
	
}
