package com.planetSimulation.ycms.services;

import java.util.ArrayList;
import java.util.Optional;

import com.planetSimulation.ycms.domains.LabResult;

public interface LabResultService {
	public LabResult save(LabResult labResult);
	Optional<LabResult> findLabResultsByResultId(long resultId);
	Optional<LabResult> findLabResultsByDoctorID(long doctorID);
	public ArrayList<LabResult> findLabResultsBySeenAndDoctorID(int seen,long doctorID);
	ArrayList<LabResult> findLabResultsBySeen(int seen);
	//public void remove(LabResult labResult);
}
