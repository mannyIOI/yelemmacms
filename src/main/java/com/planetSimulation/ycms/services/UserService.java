package com.planetSimulation.ycms.services;

import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.planetSimulation.ycms.security.User;



public interface UserService extends UserDetailsService {

	User findUserByUsername(String username);
	void saveUser(User user);
	
	Optional<User> findById(long id);
	
}
