package com.planetSimulation.ycms.services;



import java.util.ArrayList;

import com.planetSimulation.ycms.domains.Record;


public interface RecordService {
	public void saveRecord(Record record);
	
	public ArrayList<Record> findRecordByDoctorId(long doctorId);
}