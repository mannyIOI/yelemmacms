package com.planetSimulation.ycms.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.planetSimulation.ycms.domains.LabResult;
import com.planetSimulation.ycms.repositories.LabResultRepository;

@Service
public class LabResultServiceImpl implements LabResultService{

	LabResultRepository labResultRepo;
	
	public LabResultServiceImpl(LabResultRepository labResultRepository) {
		this.labResultRepo = labResultRepository;
	}
	
	@Override
	public LabResult save(LabResult labResult) {
		return labResultRepo.save(labResult);
	}

	@Override
	public Optional<LabResult> findLabResultsByResultId(long resultId) {
		// TODO Auto-generated method stub
		return labResultRepo.findLabResultsByResultId(resultId);
	}

	@Override
	public ArrayList<LabResult> findLabResultsBySeen(int seen) {
		// TODO Auto-generated method stub
		return labResultRepo.findLabResultsBySeen(seen);
	}

	@Override
	public Optional<LabResult> findLabResultsByDoctorID(long doctorID) {
		// TODO Auto-generated method stub
		return labResultRepo.findLabResultsByDoctorID(doctorID);
	}

	@Override
	public ArrayList<LabResult> findLabResultsBySeenAndDoctorID(int seen, long doctorID) {
		// TODO Auto-generated method stub
		return labResultRepo.findLabResultsBySeenAndDoctorID(seen, doctorID);
	}

//	@Override
//	public void remove(LabResult labResult) {
//		// TODO Auto-generated method stub
//		return;
//	}
}
