package com.planetSimulation.ycms.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.planetSimulation.ycms.domains.LabResult;
import com.planetSimulation.ycms.repositories.LabResultRepository;
import com.planetSimulation.ycms.security.User;
import com.planetSimulation.ycms.services.LabResultService;
import com.planetSimulation.ycms.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@SessionAttributes("labResult")
public class ViewLabResultsController {
	
	private final LabResultRepository labResultRepository;
	private LabResultService labResultService;
	private UserService userService;
	
	@Autowired
	public ViewLabResultsController(LabResultRepository labResultRepository,LabResultService labResultService,UserService userService) {
		// TODO Auto-generated constructor stub
		this.labResultRepository = labResultRepository;
		this.labResultService = labResultService;
		this.userService = userService;
	}

	@ModelAttribute(name="labResult")
	public LabResult labResult(Model model) {
		return new LabResult();
	}
	
	
	
	@GetMapping("/viewLabResults")
	public String ViewLabResults(@AuthenticationPrincipal UserDetails userDetails,Model model) {
		log.info("in get");
		
		ArrayList<LabResult> labResults = new ArrayList<LabResult>();
		String username = userDetails.getUsername();
		User user = userService.findUserByUsername(username);
		long doctorId=user.getId();
		//log.info("---------"+labResults);
		
		labResultService.findLabResultsBySeenAndDoctorID(0, doctorId).forEach(i->labResults.add(i));
		
		log.info("------"+labResults);
		model.addAttribute("labResult",labResults);
		
		return "viewLabResults";
	}
	
	@PostMapping("/viewLabResults")
	public String ResultIsSeen(LabResult labResult) {
		log.info("in post");
		log.info("----"+labResult.toString());
//		Optional<LabResult> labResultID=labResultService.findLabResultsByResultId(labResult.getResultId());
//		log.info("----"+labResultID);
//		LabResult seen = labResultID.get();
//		log.info("----"+seen);
//		seen.setSeen(1);
//		log.info("----"+labResult.getSeen());
		labResult.setSeen(1);
		labResultService.save(labResult);
		log.info("get seen-->"+labResult.getSeen());
		log.info("lab result--->"+labResult);
		return "redirect:/viewLabResults";
	}
}
