package com.planetSimulation.ycms.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.planetSimulation.ycms.domains.Appointment;
import com.planetSimulation.ycms.repositories.AppointmentRepository;
import com.planetSimulation.ycms.security.User;
import com.planetSimulation.ycms.services.AppointmentService;
import com.planetSimulation.ycms.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/book")
public class BookController{


	private AppointmentService appointmentService;
	private AppointmentRepository appointmentRepository;
	private UserService userService;
	
	
	@Autowired
	public BookController(AppointmentService appointmentService, UserService userService) {
		this.appointmentService= appointmentService;
		
		this.userService= userService;
	}
	
	
	
	
	@ModelAttribute(name="user")
	public UserDetails user(@AuthenticationPrincipal UserDetails userDetails ) {
		String username = userDetails.getUsername();
		User user = userService.findUserByUsername(username);
		return user;
		
	}
	
	@ModelAttribute(name = "appointment")
	  public Appointment appointment(Model model) {
	    return new Appointment();
	 }
	 
	
	@GetMapping
	public String showBookForm(@AuthenticationPrincipal UserDetails userDetails,@ModelAttribute Appointment appointment ) {
		
		User user = userService.findUserByUsername(userDetails.getUsername());
		log.info("user is "+ user.getFirstName());
		if(appointment.getPatientId()!=0L) {
			appointment.setPatientId(user.getId());
			
			
		}
		
		
		
		
		return "book";
	}
	
	@PostMapping
	public String processBook(@Valid Appointment appointment,@AuthenticationPrincipal UserDetails userDetails, Model model) {
		
		User user = userService.findUserByUsername(userDetails.getUsername());
		log.info("user is "+ user.getFirstName());
		appointment.setPatientId(user.getId());
		
		
		log.info("Date is "+appointment.getAppointmentDate().toString());
		log.info("patient is"+appointment.getPatientId());
		log.info("doctor id is "+appointment.getDoctorId());
		log.info("Appointment time is "+appointment.getAppointmentTime().toString());
		
		
	
		Appointment appointmentExists= appointmentService.findAppointmentByAppointmentDateAndAppointmentTimeAndDoctorId(appointment.getAppointmentDate(),appointment.getAppointmentTime(),appointment.getDoctorId());
		log.info("After appointment exist");
		if(appointmentExists!=null) {
			log.info("inside first if");
			model.addAttribute("errorMessage","Already booked");
			return "book";
			
		}
		
		
		else {
			appointmentService.save(appointment);
			model.addAttribute("successMessage", "Appointment has been booked");
			return "book";
		}
		
		
	}
	
	
	
	
	
}

