package com.planetSimulation.ycms.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.planetSimulation.ycms.security.User;
import com.planetSimulation.ycms.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class SignupController {

	@Autowired
	private UserService userService;
	
	@ModelAttribute(name="user")
	public User user(Model model) {	
		return new User();
	}
	
	@GetMapping("/signup")
	public String signup() {
		return "signup";
	}
	
	@PostMapping("/signup")
    public String createNewUser(@Valid User user, BindingResult bindingResult, Model model) {
        User userExists = userService.findUserByUsername(user.getUsername());

        if (userExists != null) {
        	 model.addAttribute("errorMessage","Username already exists");
        	 return "signup";
            
        }
        else {
        	
            userService.saveUser(user);
            
            model.addAttribute("successMessage", "User has been registered successfully");
            
            return "redirect:/login";
        }
    }
}

