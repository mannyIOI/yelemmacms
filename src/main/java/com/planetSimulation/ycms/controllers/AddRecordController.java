package com.planetSimulation.ycms.controllers;


import java.util.Calendar;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import com.planetSimulation.ycms.domains.Record;
import com.planetSimulation.ycms.repositories.RecordRepository;
import com.planetSimulation.ycms.security.User;
import com.planetSimulation.ycms.services.RecordService;
import com.planetSimulation.ycms.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/addRecord")
public class AddRecordController {

	

	private RecordService recordService;
	private UserService userService;
	
	@Autowired
	public AddRecordController(RecordService recordService,UserService userService) {
		this.recordService= recordService;
		this.userService= userService;

	}
	@ModelAttribute(name="user")
	public UserDetails user(@AuthenticationPrincipal UserDetails userDetails ) {
		String username = userDetails.getUsername();
		User user = userService.findUserByUsername(username);
		return user;
		
	}
	
	@ModelAttribute(name = "record")
	public Record record(Model model) {
	    return new Record();
	}
	
	
	@GetMapping
    public String showRecords(@AuthenticationPrincipal UserDetails userDetails, @ModelAttribute Record record ) {	
		
		
		return "add_record";
	}
	
	@PostMapping
    public String addrecords(@Valid Record record,@AuthenticationPrincipal UserDetails userDetails, Model model) {
		User user = userService.findUserByUsername(userDetails.getUsername());
		log.info("user is "+ user.getFirstName());
		record.setDoctorId(user.getId());
		log.info("Doctor id is "+ record.getDoctorId());
		Date entryDate = new Date(Calendar.getInstance().getTimeInMillis());
		record.setEntryDate(entryDate);
		
        	
            recordService.saveRecord(record);
            return "add_record";
        }
}