package com.planetSimulation.ycms.controllers;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.planetSimulation.ycms.domains.LabExam;
import com.planetSimulation.ycms.domains.LabResult;
import com.planetSimulation.ycms.repositories.LabResultRepository;
import com.planetSimulation.ycms.security.User;
import com.planetSimulation.ycms.services.LabResultService;
import com.planetSimulation.ycms.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/addResult")
@SessionAttributes("labResult")
public class AddResultController {

	private final LabResultRepository labResultRepository;
	private UserService userService;
	private LabResultService labResultService;
	
	@Autowired
	public AddResultController(UserService userService,LabResultRepository labResultRepository,LabResultService labResultService) {
		this.labResultRepository = labResultRepository;
		this.userService = userService;
		this.labResultService = labResultService;
	}
	
	@GetMapping
	public String addResult(@ModelAttribute LabResult labResults) {
		
		log.info("lab resulttt is "+ labResults.toString());
		
		
		return "add_results";
	}
	
	@PostMapping
	public String postResult(@Valid LabResult labResult,BindingResult bindingResult,Model model,SessionStatus sessionStatus) {
		log.info("in post");
		log.info("inputs---"+labResult.getDoctorID()+labResult.getPatientID()+labResult.getData());
		log.info("labResult---"+labResult);
		
		labResultService.save(labResult);
		
		log.info("labresult saved");
		model.addAttribute("successMessage","Result has been posted!");
		sessionStatus.setComplete();
		return "redirect:/viewLab";
	}
}

