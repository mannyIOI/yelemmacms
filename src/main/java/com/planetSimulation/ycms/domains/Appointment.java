package com.planetSimulation.ycms.domains;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;



@Data
@Entity

@Table(name="appointment",schema="ycms")
public class Appointment {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long appointmentId;
	
	@Column(name="patient_id")
	private long patientId;
	
	@Column(name="doctor_id")
	private long doctorId;
	  
	@DateTimeFormat(pattern="HH:mm")
	@Temporal(value = TemporalType.TIME )
	@Column(name="appointment_time")
	private Date appointmentTime;
	  
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(value = TemporalType.DATE )
	@Column(name="appointment_date")
	private Date appointmentDate;
	
	@Column(name="approved")
	private int approved;

}

