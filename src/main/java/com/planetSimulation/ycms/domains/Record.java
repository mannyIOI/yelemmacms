package com.planetSimulation.ycms.domains;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Entity
@Data
@Table(name="record",schema="ycms")
public class Record {
	
	@Id
	private long recordId;
	
	@Column(name="patient_id")
	private long patientId;
	
	@Column(name="doctor_id")
	private long doctorId;
	
	@Column(name="data")
	private String data;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(value = TemporalType.DATE )
	@Column(name="recordDate")
	private Date entryDate;
}
