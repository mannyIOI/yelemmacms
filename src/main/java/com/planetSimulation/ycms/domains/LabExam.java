package com.planetSimulation.ycms.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="lab_exam",schema="ycms")
public class LabExam {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long labExamId;
	
	@Column(name="patient_id")
	private long patientId;
	
	@Column(name="doctor_id")
	private long doctorId;
	
	@Column(name="test_type")
	private String testType;
	
	@Column(name="approved")
	private int approved;
	


}

