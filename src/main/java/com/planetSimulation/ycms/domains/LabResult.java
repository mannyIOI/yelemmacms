package com.planetSimulation.ycms.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
@Entity
@Table(name="result",schema="ycms")
public class LabResult {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long resultId;
	
	@Column(name="patient_id")
	private long patientID;
	
	@Column(name="doctor_id")
	private long doctorID;
	
	@Column(name="data")
	private String data;
	
	@Column(name="seen")
	private int seen;
}
