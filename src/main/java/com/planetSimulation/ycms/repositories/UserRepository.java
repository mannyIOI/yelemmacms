package com.planetSimulation.ycms.repositories;

import org.springframework.data.repository.CrudRepository;

import com.planetSimulation.ycms.security.User;



public interface UserRepository extends CrudRepository<User, Long>{
	
	User findByUsername(String username);
	
}
