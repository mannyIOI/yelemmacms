package com.planetSimulation.ycms.repositories;

import org.springframework.data.repository.CrudRepository;

import com.planetSimulation.ycms.security.Role;



public interface RoleRepository extends CrudRepository<Role, Long>{

	 Role findByRole(String role);
	 
}
