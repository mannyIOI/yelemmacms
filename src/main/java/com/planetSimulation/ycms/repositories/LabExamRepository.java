package com.planetSimulation.ycms.repositories;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

import com.planetSimulation.ycms.domains.LabExam;

public interface LabExamRepository extends CrudRepository<LabExam, Long> {

	ArrayList<LabExam> findLabExamByApproved(int approved);
}
