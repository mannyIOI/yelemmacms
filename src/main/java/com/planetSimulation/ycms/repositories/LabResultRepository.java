package com.planetSimulation.ycms.repositories;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.planetSimulation.ycms.domains.LabResult;

public interface LabResultRepository extends CrudRepository<LabResult, Long>{
	//public Iterable<LabResult> findAllByResultID(long resultID);

	public Optional<LabResult> findLabResultsByResultId(long resultId);
	
	public Optional<LabResult> findLabResultsByDoctorID(long doctorID);
	
	public ArrayList<LabResult> findLabResultsBySeenAndDoctorID(int seen,long doctorID);

	public ArrayList<LabResult> findLabResultsBySeen(int seen);

	//public void remove(LabResult labResult);
}
