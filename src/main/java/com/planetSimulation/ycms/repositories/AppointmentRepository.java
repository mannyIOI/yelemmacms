package com.planetSimulation.ycms.repositories;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.planetSimulation.ycms.domains.Appointment;

public interface AppointmentRepository extends CrudRepository<Appointment, Long> {
	
	public Iterable<Appointment> findAllByAppointmentDate(Date appointmentDate);
	
	public Iterable<Appointment> findAllByDoctorId(long doctorId);
	Appointment findAppointmentByAppointmentDateAndAppointmentTimeAndDoctorId(Date appointmentDate,Date appointmentTime,long doctorId );
	
	public ArrayList<Appointment> findAppointmentByAppointmentDateAndDoctorIdAndApproved(Date appointmentDate,long doctorId, int approved);
	
	public ArrayList<Appointment> findAppointmentByAppointmentDateAndApproved(Date appointmentDate, int approved);

	
	
}

