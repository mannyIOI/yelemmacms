package com.planetSimulation.ycms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;



//@ComponentScan(basePackages="com.planetSimulation.ycms,com.planetSimulation.ycms.controllers,com.planetSimulation.ycms.data,com.planetSimulation.ycms.services")
@SpringBootApplication
public class YcmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(YcmsApplication.class, args);
	}
}
