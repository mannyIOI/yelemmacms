
    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)
<<<<<<< HEAD

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)
=======

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)
>>>>>>> branch 'master' of https://bekathekiller@bitbucket.org/planetsimulation/yelemmacms.git

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)

    create table appointment (
       appointment_id bigint not null auto_increment,
        appointment_date date,
        appointment_time time,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        primary key (appointment_id)
    ) engine=InnoDB

    create table lab_exam (
       lab_exam_id bigint not null auto_increment,
        approved integer,
        doctor_id bigint,
        patient_id bigint,
        test_type varchar(255),
        primary key (lab_exam_id)
    ) engine=InnoDB

    create table record (
       record_id bigint not null,
        data varchar(255),
        doctor_id bigint,
        record_date date,
        patient_id bigint,
        primary key (record_id)
    ) engine=InnoDB

    create table result (
       result_id bigint not null auto_increment,
        data varchar(255),
        doctor_id bigint,
        patient_id bigint,
        seen integer,
        primary key (result_id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_data (
       id bigint not null auto_increment,
        age integer not null,
        email varchar(255),
        user_fname varchar(255),
        user_image tinyblob,
        user_lname varchar(255),
        password varchar(255),
        phone_number varchar(255),
        sex varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FKp22died80dx5melwhyfwxbkcn 
       foreign key (user_id) 
       references user_data (id)
